package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"path/filepath"

	"gopkg.in/yaml.v2"

	"os"
)

type ConfigEntry struct {
	Url        string `yaml:"url"`
	StatusCode *int   `yaml:"statusCode"`
}

type Config struct {
	Entries map[string][]*ConfigEntry `yaml:"entries"`
}

func main() {
	filename, _ := filepath.Abs("./configuration/connectivity-links.yaml")
	yamlFile, err := ioutil.ReadFile(filename)

	if err != nil {
		panic(err)
	}

	var config Config

	err = yaml.Unmarshal(yamlFile, &config)
	if err != nil {
		panic(err)
	}

	fmt.Println(config)

	env := os.Getenv("CLUSTER_ENV")
	// default value
	if env == "" {
		env = "dev"
	}

	links := config.Entries[env]
	fmt.Println(links)

	// loop trough all links per env
	for _, v := range links {
		fmt.Printf("-------%v--------\n", v.Url)
		resp, err := http.Get(v.Url)
		if err != nil {
			fmt.Println(err)
		} else {
			fmt.Printf("URL: %v, Status Code: %v\n", v.Url, resp.StatusCode)
		}
		fmt.Printf("Expected Status Code: %v\n", *v.StatusCode)
	}
}

func HttpGetRequest(url string) (int, error) {
	fmt.Println(url)
	resp, err := http.Get(url)
	var statusCode int
	if err != nil {
		fmt.Println(err)
		statusCode = -1
	} else {
		fmt.Printf("URL: %v, Status Code: %v\n", url, resp.StatusCode)
		statusCode = resp.StatusCode
	}
	return statusCode, err
}
