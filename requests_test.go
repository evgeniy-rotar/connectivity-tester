package main

import (
	"io/ioutil"
	"path/filepath"
	"testing"

	"gopkg.in/yaml.v2"
)

func TestDevLinks(t *testing.T) {
	filename, _ := filepath.Abs("./configuration/connectivity-links.yaml")
	yamlFile, err := ioutil.ReadFile(filename)

	if err != nil {
		panic(err)
	}

	var config Config

	err = yaml.Unmarshal(yamlFile, &config)
	if err != nil {
		panic(err)
	}

	links := config.Entries["dev"]

	for _, v := range links {
		statusCode, err := HttpGetRequest(v.Url)
		if err != nil {
			t.Error(err)
		}

		if *v.StatusCode != statusCode {
			t.Errorf("%v: expected: %v, actual: %v", v.Url, *v.StatusCode, statusCode)
		}
	}
}

func TestStagingLinks(t *testing.T) {
	filename, _ := filepath.Abs("./configuration/connectivity-links.yaml")
	yamlFile, err := ioutil.ReadFile(filename)

	if err != nil {
		panic(err)
	}

	var config Config

	err = yaml.Unmarshal(yamlFile, &config)
	if err != nil {
		panic(err)
	}

	links := config.Entries["staging"]

	for _, v := range links {
		statusCode, err := HttpGetRequest(v.Url)
		if err != nil {
			t.Error(err)
		}

		if *v.StatusCode != statusCode {
			t.Errorf("%v: expected: %v, actual: %v", v.Url, *v.StatusCode, statusCode)
		}
	}
}
